%rebase("base.tpl",title="To Do List")


<article>
    <div class="my_profile_container">
        <div class="my_profile_info">
                
            <h2 class="prof-username"> {{user_info[0]}}さんのページ</h2>
            <img src="static/img/{{user_info[1]}}" alt="" class="mypage_prof_img">

        </div>
        
        <form action="/upload" method="post" enctype="multipart/form-data">
            <dir class="file_box">
                <input class = "mypage_input_file" type="file" name="upload" accept="image/*"><br>
            </dir>
            <input class = "mypage_input_submit" type="submit" value="画像変更"><br>
        </form>


    </div>

        <h2 class="content-title" >作成したスレッド</h2>
        % if not title_info:
            <div class="container">
                <div class="thread_top_container">
                        <div class="thread_title_container">
                            <h2>作成したリストはまだありません</h2>
                        </div>
                    </div>
                <img src="/static/img/aisatsu_kodomo_boy.png" alt="" class="gomen">
                    
            </div>
        % else: 
            % for item in title_info:
            <div class="container">
                <div class="thread_top_container">
                    <div class="thread_title_container">
                        <h2>{{item['list_title']}}</h2>
                    </div>
                </div>
                <div class="more-detail-link">
                    <a  href="/list/{{item['id']}}">もっと見る…</a>
                </div>
                    
            </div>
            % end
        % end

        <h2 class="content-title" >いいねスレッド</h2>
        % if not user_like_info:
            <div class="container">
                <div class="thread_top_container">
                        <div class="thread_title_container">
                            <h2>いいねしたスレッドはまだありません</h2>
                        </div>
                    </div>
                <img src="/static/img/aisatsu_kodomo_boy.png" alt="" class="gomen">
                    
            </div>
        % else: 
            % for item in user_like_info:
            <div class="container">
                <div class="thread_top_container">
                    <div class="thread_title_container">
                        <h2>{{item['thread_title']}}</h2>
                    </div>
                </div>
                <div class="more-detail-link">
                    <a  href="/list/{{item['title_id']}}">もっと見る…</a>
                </div>
                    
            </div>
            % end
        % end

	</article>