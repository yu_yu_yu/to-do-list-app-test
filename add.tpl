%rebase("base.tpl",title="To Do List")

<form action="/list/new" method="post" enctype="multipart/form-data">
    <img class="add_thread_icon" src="/static/img/add_thread_img.png" alt="">
    <div class="adding-thread-container" >
        <h2>スレッドの作成</h2>
    </div>
    
    <textarea class="add_thread_box" name="comment" cols="30" rows="3" placeholder="スレッドの内容を入力して下さい"></textarea><br>

    <input type="file" name="upload" accept="image/*"><br>
    <div class="adding-list-container">
        <h2>To Do リストへの追加</h2>
        <input class="list-input" name="list1" type="text" placeholder="リスト１"><br>
        <input class="list-input" name="list2" type="text" placeholder="リスト２"><br>
        <input class="list-input" name="list3" type="text" placeholder="リスト３"><br>
    </div>

    <input class="btn" type="submit" name="save" value="作成">


</form>
