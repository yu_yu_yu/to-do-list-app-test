<!DOCTYPE HTML>
<html lang="ja">

<head>
	<meta charset="utf-8">
	<meta name="keywords" content="サイトをキーワードで説明">
	<meta name="description" content="どんなサイトかを短い文章で説明">
	<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=1">
	<link rel="icon" href="staic/favicon.ico">
	<link rel="stylesheet" type="text/css" media="screen and ( min-width:769px )" href="/static/css/style_pc.css">
	<link rel="stylesheet" type="text/css" media="screen and ( max-width:768px )" href="/static/css/style_sp.css">
	<title>{{title or 'No title'}}</title>

<script type="text/javascript" src="/static/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="/static/js/jquery.fadethis.min.js"></script>
<script type="text/javascript" src="/static/js/footerFixed.js"></script>
<!-- drawer.css -->
<link rel="stylesheet" href="/static/css/drawer.min.css">
<!-- jquery & iScroll -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/iScroll/5.2.0/iscroll.min.js"></script>

<!-- drawer.js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/drawer/3.2.2/js/drawer.min.js"></script>

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
  
<script type="text/javascript" src="/static/js/common.js"></script>
<!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script> -->

</head>

<body class="drawer drawer--right">

		<div class="add-list" >
				<a href="/list/new" class="add_list_btn"><img class="add_icon" src="static/img/add-botan.png" alt=""></a>
		</div>

	<header>
		<div class="top_container">
			<div class="top_container_left">
				<a href="/"><img class="top_prof_img" src="/static/img/{{user_info[1]}}" alt=""></a> 
			</div>
			<div class="search_container">
				<form action="/search" method="GET" id="searchbox">
					<input class="search-box" type="search" name="search" placeholder="キーワードを入力">
					<button type="submit" id="sbtn2"><i class="fas fa-search"></i></button>
				</form>
			</div>
			<div class="top_container_rigth">
				<button type="button" class="drawer-toggle drawer-hamburger" id = "drawer-hamburger">
					<span class="sr-only">toggle navigation</span>
					<span class="drawer-hamburger-icon"></span>
				</button>
			</div>

		</div>
	<nav class="drawer-nav" role="navigation">
		<img class="drawer-img" src="/static/img/logo-text-2.png" alt="">
		<ul class="drawe-menu">
			% if user_info[0] == "Guest":
				<li>
					<a class="drawer-menu-item" href="/register">ご登録はこちら</a>
				</li>
				<li>
					<a class="drawer-menu-item" href="/login">ログイン</a>
				</li>
			% else:
				<li>
					<a class="drawer-menu-item" href="/mypage">マイページ</a>
				</li>
				<li>
					<a class="drawer-menu-item" href="/logout">ログアウト</a>
				</li>
			% end
		</ul>
	</nav>
	
	</header>

{{!base}}

  <footer id="footer">
		<p class="copyright">Copyright &copy; DEMO all rights reserved.</p>
	</footer>

</body>

</html>
