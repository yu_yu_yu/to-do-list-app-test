%rebase("base.tpl",title="To Do List")

<article>
		<h2>詳しいリスト {{user_info[0]}}さん、ようこそ</h2>
		% for item in title_info:
		<div class="container">
			<div class="thread_top_container">
				<div class="thread_icon_container">
                        <img  src="/static/img/{{item['user_img_path']}}" alt="">
                </div>
                <div class="thread_title_container">
                    <h2>{{item['list_title']}}</h2>
                </div>
            </div>

			% if item["thraed_img_path"] !="":
				<img class = "thread_img" src="/static/img/{{item['thraed_img_path']}}"  alt="">
            % end
            <p>Created: {{item['created_time']}}</p>
            <form action="/add_list" method="post" class="add_list_form">
                <textarea class="add_thread_box" name="comment" cols="30" rows="3" placeholder="リスト追加内容"></textarea><br>
                <input type="hidden" name="title_id" value="{{item['id']}}">
                <input  class="btn" type="submit" name="save" value="追加">
            </form>

			% if item["list_info"] !="":
			<div class="thread_list_container">
                  <ol>
                    % index = 1
                    % for content in item["list_info"]:
                        
                        <li class="each-list-content" >
                            % if index <= 3:
                                <img class="rank-img" src="/static/img/rank_{{index}}.png" alt="">
                            % else:
                                <span class="extra_space"></span>
                            % end

                            
                            {{content["list_content"]}}
                            <form class="form-signin" action="/list" method="post" role="form">
                                <div class="heart-box" value="{{content['list_id']}}" > 
                                    % if content["like_check"] == 1:
                                        <i class="fas fa-heart fa like-active"></i>
                                    % else:
                                        <i class="fas fa-heart fa"></i>
                                    % end
                                    <span class="displayValue">{{content['like_count']}}</span> 
                                </div> 
                            </form>
                        </li> 
                        % index +=1
                    % end
                </ol>
			</div>
			
		</div>
        % end
</article>





    