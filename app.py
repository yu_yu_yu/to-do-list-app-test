import os, sys
import sqlite3
import secrets, datetime
from bottle import route, run, debug, template, request, static_file, error, redirect, response
import json
from json import dumps
secret_cookie = secrets.token_hex()

# @route('/',method=["GET"])
# def index():
#     user_id = request.get_cookie("user_id" , secret=secret_cookie)
#     if user_id is None:
#         user_info = ["Guest","default_icon.png"]
#         return template("index", user_info=user_info)
#     else:
#         conn = sqlite3.connect('service.db')
#         c = conn.cursor()
#         c.execute("select username, prof_img_path from users where id = ?", (user_id,))
#         user_info = c.fetchone()
#         print(user_info)
#         conn.commit()

        
#         conn.close()
#         return template('index',user_info=user_info)

@route('/',method=["GET"])
def get_lists():
    user_id = request.get_cookie("user_id" , secret=secret_cookie)
    if user_id is None:
        user_info = ["Guest","default_icon.png"]
    else:
        conn = sqlite3.connect('service.db')
        c = conn.cursor()
        c.execute("select username, prof_img_path from users where id = ?", (user_id,))
        user_info = c.fetchone()
        print(user_info)
        conn.commit()
        conn.close()

    conn = sqlite3.connect('service.db')
    c = conn.cursor()
    d = conn.cursor()
    e = conn.cursor()
    c.execute("select* from title_info order by created_time DESC;")
    # d.execute("select count(*),title_info.id from lists join title_info on title_info.id = lists.thread_id group by title_info.id")
    title_info = []
    for row in c.fetchall():
        # print("---------------------------")
        # print(row[0])
        e.execute("select thread_content from lists where thread_id = ? limit 3;", (row[0],))

        d.execute("select users.prof_img_path from users join title_info on users.id = title_info.user_id where title_info.id = ? ", (row[0],))
        user_prof_path = d.fetchone()
        # print("user_img pass",user_prof_path[0])
        temp_list =[]
        for row1 in e.fetchall():
            # print("row1",row1[0])
            temp_list.append(row1[0])
        # print("temp_list",temp_list)
        title_info.append({"id": row[0], "list_title": row[1], "created_time":row[2],"thraed_img_path":row[3], "user_id":row[4],"list_content":temp_list, "user_img_path": user_prof_path[0]})
    conn.close()
    # print("title_info",title_info)
    # print(title_info[0]["list_content"][0])

    return template('tempindex',user_info=user_info,title_info=title_info)


# GET  /register => 登録画面を表示
# POST /register => 登録処理をする
@route('/register',method=["GET", "POST"])
def register():
    if request.method == "GET":
        name = request.get_cookie("user_id" , secret=secret_cookie)
        if name is None:
            user_info = ["Guest","default_icon.png"]
            return template("register", user_info=user_info)
        else:
            return redirect("/")
    # ここからPOSTの処理
    else:
        name = request.POST.getunicode("name")
        password = request.POST.getunicode("password")
        created_time = datetime.datetime.now().strftime('%Y/%m/%d %H:%M:%S')
        print("username",name)

        conn = sqlite3.connect('service.db')
        c = conn.cursor()
        c.execute("insert into users values(null,?,?,?,'default_icon.png')", (name,password,created_time))
        conn.commit()
        conn.close()
        return redirect('/login')


# GET  /login => ログイン画面を表示
# POST /login => ログイン処理をする
@route("/login", method=["GET", "POST"])
def login():
    if request.method == "GET":
        user_id = request.get_cookie("user_id", secret=secret_cookie)
        if user_id is None:
            user_info = ["Guest","default_icon.png"]
            return template("login",user_info=user_info)
        else:
            print("login if if else")
            return redirect("/")
    else:
        # ブラウザから送られてきたデータを受け取る
        name = request.POST.getunicode("name")
        password = request.POST.getunicode("password")

        # ブラウザから送られてきた name ,password を userテーブルに一致するレコードが
        # 存在するかを判定する。レコードが存在するとuser_idに整数が代入、存在しなければ nullが入る
        conn = sqlite3.connect('service.db')
        c = conn.cursor()
        c.execute("select id from users where username = ? and password = ?", (name, password) )
        user_id = c.fetchone()
        print(user_id)
        conn.close()

        # user_id が NULL(PythonではNone)じゃなければログイン成功
        if user_id is not None:
            user_id = user_id[0]
            # クッキー(ブラウザ側に)にnameを記憶させる
            # これで誰が今ログインしているのか判定できる
            response.set_cookie("user_id", user_id, secret=secret_cookie)
            print("aaaaa")
            return redirect("/")
        else:
            # ログイン失敗すると、ログイン画面に戻す
            print("bbbb")            
            user_info = ["Guest","default_icon.png"]
            return template("login",user_info=user_info)

@route("/logout")
def logout():
    # ログアウトはクッキーに None を設定してあげるだけ
    response.set_cookie("user_id", None, secret=secret_cookie)
    return redirect("/") # ログアウト後はログインページにリダイレクトさせる


@route('/mypage')
def mypage():
    # クッキーからuser_idを取得
    user_id = request.get_cookie("user_id" , secret=secret_cookie)
    
    if user_id is None:
        # user_info = ["Guest","default_icon.png"]
        # return template("",user_info=user_info)
        return redirect("/")
    else:
        conn = sqlite3.connect('service.db')
        c = conn.cursor()
        c.execute("select username, prof_img_path from users where id = ?", (user_id,))
        user_info = c.fetchone()

        f = conn.cursor()
        d = conn.cursor()
        e = conn.cursor()
        f.execute("select* from title_info where user_id = ? order by created_time DESC;",(user_id,))
        d.execute("select count(*),title_info.id from lists join title_info on title_info.id = lists.thread_id group by title_info.id")
        count  = d.fetchall()
        print(count)
        title_info = []
        for row in f.fetchall():
            # print(row[0])
            e.execute("select thread_content from lists where thread_id = ? ", (row[0],))
            # temp_list = e.fetchall()
            temp_list =[]
            for row1 in e.fetchall():
                temp_list.append(row1[0])
                # print("a")
                # print("contents",row1[0])
            # print("inside the list",temp_list)
            # print(type(temp_list))
            # title_info.append({"id": row[0], "list_title": row[1], "created_time":row[2],"thraed_img_path":row[3], "user_id":row[4]}
            # for row1 in e.fetchall():
            print(temp_list)
            title_info.append({"id": row[0], "list_title": row[1], "created_time":row[2],"thraed_img_path":row[3], "user_id":row[4],"list_content":temp_list})
        conn.close()
        if not title_info :
            print("Title info is empty")
        else:
            print("title_info is NOT empty")
            
        # print(title_info[0]["list_content"][0])
    # いいねしたスレッドのidとそのスレタイ を取得
    conn = sqlite3.connect('service.db')
    c = conn.cursor()
    c.execute("select like_lists.title_id, title_info.list_title from like_lists join title_info on title_info.id = like_lists.title_id where like_lists.user_id = ? group by title_id;", (user_id,))
    user_like_info =[]
    for item in c.fetchall():
        user_like_info.append({"title_id": item[0], "thread_title": item[1]})
    if user_like_info !='':
        print(user_like_info)

    return template('mypage',user_info=user_info,title_info=title_info , user_like_info = user_like_info)

        # conn.close()
        # return template('mypage',user_info=user_info)


#プロフィール画像をアップする
@route('/upload', method='POST')
def do_upload():
    # bbs.tplのinputタグ name="upload" をgetしてくる
    user_id = request.get_cookie("user_id" , secret=secret_cookie)
    upload = request.files.get('upload')
    # upload = request.files.get('upload', '')
    if not upload.filename.lower().endswith(('.png', '.jpg', '.jpeg')):
        return 'png,jpg,jpeg形式のファイルを選択してください'
        # パスを取ってくる、下のdef get_save_path():
        # imgフォルダに画像を保存する必要あり。パスをつなごう。関数作ろう。
    save_path = get_save_path()
    print("File path",save_path)
# imgファイルをstatic/imgフォルダに保存
    # os.remove(save_path+upload.filename)
    upload.save(save_path)
    # ファイル名が取れることを確認、あとで使うよ
    print(upload.filename)

    # アップロードしたユーザのIDを取得
    conn = sqlite3.connect('service.db')
    c = conn.cursor()
    # update文 
#  (upload.filename) ここで使うよ
    print("prof image file name",upload.filename)
    print("user_id",user_id)
    c.execute("update users set prof_img_path = ? where id=?", (upload.filename,user_id))
    conn.commit()
    conn.close()

    return redirect ('/mypage')


@route('/list/new', method=["GET"])
def add():
    # クッキーから user_id を取得
    user_id = request.get_cookie("user_id", secret=secret_cookie)
    if user_id is None:
        user_info = ["Guest","default_icon.png"]
        return template("add",user_info=user_info)
    else:
        conn = sqlite3.connect('service.db')
        c = conn.cursor()
        c.execute("select username, prof_img_path from users where id = ?", (user_id,))
        user_info = c.fetchone()
        conn.commit()
        conn.close()
        return template('add',user_info  = user_info)
        
#新しいスレッドの作成
@route('/list/new', method=["POST"])
def create_list():
    # クッキーから user_id を取得
    user_id = request.get_cookie("user_id", secret=secret_cookie)
    created_time = datetime.datetime.now().strftime('%Y/%m/%d %H:%M:%S')
    # POSTアクセスならDBに登録する
    # フォームから入力されたアイテム名の取得(Python2ならrequest.POST.getunicodeを使う)
    title = request.forms.get("comment")
    # print(vars(list_title)
    upload = request.files.get('upload')
    img_path=""
    print("Title",title)
    if upload is None:
        print("No image File")
    else:
        print(upload.filename)
        save_path = get_save_path()
        # imgファイルをstatic/imgフォルダに保存
        upload.save(save_path)
        # ファイル名が取れることを確認、あとで使うよ
        img_path =upload.filename
        print(save_path+upload.filename)

    #リストの取得
    lists=[]
    lists.append( request.forms.get("list1"))
    lists.append( request.forms.get("list2"))
    lists.append( request.forms.get("list3"))
    #リストが空の場合、その要素をリストから取り除く
    print(lists)
    lists = list(filter(lambda str: str != "", lists))
    
    if not lists:
        print("EMPTY")
    else:
        print("Not Empty")
    
    #タイトルデータの保存
    conn = sqlite3.connect('service.db')
    c = conn.cursor()
    c.execute("insert into title_info values(null,?,?,?,?)", (title,created_time,img_path,user_id))
    conn.commit()

    # listに書き出し
    c.execute("select id from title_info where list_title = ? and user_id =? and created_time=?;", (title,user_id,created_time,))
    list_id = c.fetchone()[0]
    print(list_id )
    print(len(lists))
    for item in lists:
        print(item)
        c.execute("insert into lists values(null,?,?,?,?,0,0)", (list_id,item,user_id,created_time))
    conn.commit()
    conn.close()
       
    # print (lists[0])
    # conn = sqlite3.connect('service.db')
    # c = conn.cursor()
    # # DBにデータを追加する
    # c.execute("insert into bbs values(null,?,?)", (user_id, comment))
    # conn.commit()
    # conn.close()
    return redirect('/')

# 選択したスレッドの詳細リストを表示する
@route('/list/<id:int>')
def edit(id):
    user_id = request.get_cookie("user_id" , secret=secret_cookie)
    print("test, user id",user_id)
    if user_id is None:
        user_info = ["Guest","default_icon.png"]
    else:
        conn = sqlite3.connect('service.db')
        c = conn.cursor()
        c.execute("select username, prof_img_path from users where id = ?", (user_id,))
        user_info = c.fetchone()
        print(user_info)
        conn.commit()
        conn.close()
    conn = sqlite3.connect('service.db')
    c = conn.cursor()
    d = conn.cursor()
    e = conn.cursor()
    f = conn.cursor()
    # id から該当するスレッド情報を得る
    c.execute("select* from title_info where id = ?;", (id,))   
    title_info = []

    for row in c.fetchall():
        # print(row[0])
        e.execute("select id, thread_content, like_count from lists where thread_id = ? order by like_count DESC", (row[0],))
        d.execute("select users.prof_img_path from users join title_info on users.id = title_info.user_id where title_info.id = ? ", (row[0],))
        user_prof_path = d.fetchone()
        # print("user_img pass",user_prof_path[0])

        list_info =[]
        for row1 in e.fetchall():
            f.execute(" select list_id from like_lists where user_id = ? and list_id =?",(user_id, row1[0]))
            check_like_list = f.fetchone()
            print(check_like_list)
            if check_like_list ==None:
                print("list id",row1[0],"NOT like")
                list_info.append({"list_id":row1[0],"list_content":row1[1],"like_count":row1[2],"like_check":0})
            else:
                print("list id",row1[0],"like")
                list_info.append({"list_id":row1[0],"list_content":row1[1],"like_count":row1[2],"like_check":1})
         
        # print(list_info)
        title_info.append({"id": row[0], "list_title": row[1], "created_time":row[2],"thraed_img_path":row[3], "user_id":row[4],"list_info":list_info, "user_img_path": user_prof_path[0]})
    conn.close()
    # print("title_info",title_info[0])
    # print(title_info[0]["list_content"][0])

    return template('list',user_info=user_info,title_info=title_info)
    # for row in c.fetchall():
    # # print(row[0])
    # e.execute("select thread_content from lists where thread_id = ? ", (row[0],))
    # d.execute("select users.prof_img_path from users join title_info on users.id = title_info.user_id where title_info.id = ? ", (row[0],))
    # user_prof_path = d.fetchone()
    # print("user_img pass",user_prof_path[0])
    # temp_list =[]
    # for row1 in e.fetchall():
    #     temp_list.append(row1[0])
    # print(temp_list)
    # title_info.append({"id": row[0], "list_title": row[1], "created_time":row[2],"thraed_img_path":row[3], "user_id":row[4],"list_content":temp_list, "user_img_path": user_prof_path[0]})
    # conn.close()
    # print("title_info",title_info[0])
    # # print(title_info[0]["list_content"][0])

    # return template('list',user_info=user_info,title_info=title_info)
@route('/search', method=["GET"])
def search_word():

    user_id = request.get_cookie("user_id" , secret=secret_cookie)
    if user_id is None:
        user_info = ["Guest","default_icon.png"]
    else:
        conn = sqlite3.connect('service.db')
        c = conn.cursor()
        c.execute("select username, prof_img_path from users where id = ?", (user_id,))
        user_info = c.fetchone()
        conn.commit()
        conn.close()

    search_word = request.GET.getunicode("search")
    print("Search Word",search_word)
    conn = sqlite3.connect('service.db')
    c = conn.cursor()
    c.execute("select * from title_info where list_title like ?;",('%'+search_word+'%',))
    title_info = []
    for row in c.fetchall():
        title_info.append({"id": row[0], "list_title": row[1]})
    conn.close()
    #検索数
    search_count = len(title_info)

    return template('search',user_info  = user_info, title_info=title_info, search_word = search_word,search_count=search_count)


@route("/likeCount", method=["POST"])
def get_like_info():
    user_id = request.get_cookie("user_id" , secret=secret_cookie)

    if request.headers.get('X-Requested-With') == 'XMLHttpRequest':
        print('AJAXXXXXXXXXXX')

    # ajaxを使ってデータを受け取る
    like_flag= request.json["like_flag"]
    title_id = request.json["title_id"]
    list_id = request.json["list_id"]
    # requJason = request.json['text']
    print("list-id",list_id)
    print("like-flag",like_flag)
    print("title-id",title_id)
    if user_id is None:
        return json.dumps({'status':'Guest'})
    print("user-id",user_id)
    # requJason = request.json['text']
    # print("thread-id",requJason)
# DBに接続
    conn = sqlite3.connect('service.db')
    c = conn.cursor()
    # like_listに追加・削除
    d = conn.cursor()
    if like_flag ==1:
        c.execute("update lists set like_count = like_count+1 where id = ?;",(list_id,))
        d.execute("insert into like_lists values(null,?,?,?)", (user_id, title_id,list_id))
    else:
        c.execute("update lists set like_count = like_count-1 where id = ?;",(list_id,))
        d.execute("delete from like_lists where list_id = ? and user_id=?;",(list_id,user_id))
    conn.commit()
    conn.close()
    
    return json.dumps({'status':'updated'})

# @route("/list")
# def update_item():
#     # ブラウザから送られてきたデータを取得
#     item_id = request.GET.getunicode("item_id") # id
#     item_id = int(item_id)# ブラウザから送られてきたのは文字列なので整数に変換する
#     comment = request.GET.getunicode("comment") # 編集されたテキストを取得する

#     # 既にあるデータベースのデータを送られてきたデータに更新
#     timeis = datetime.datetime.now()
#     nowtime = timeis.strftime('%Y/%m/%d %H:%M:%S')
#     conn = sqlite3.connect('service.db')
#     c = conn.cursor()
#     c.execute("update bbs set comment = ? tims = ? where id = ?",(comment,nowtime, item_id))
#     conn.commit()
#     conn.close()

#     # アイテム一覧へリダイレクトさせる
#     return redirect("/bbs")


def get_save_path():
    path_dir = "./static/img/"
    return path_dir

@route('/tempPost', method=["GET"])
def show_list():
    # user_id = request.get_cookie("user_id", secret=secret_cookie)
    conn = sqlite3.connect('service.db')
    c = conn.cursor()
    # # DBにアクセスしてログインしているユーザ名と投稿内容を取得する
    # クッキーから取得したuser_idを使用してuserテーブルのnameを取得
    c.execute("select* from  where id = ?", (user_id,))
    # fetchoneはタプル型
    user_name = c.fetchone()[0]
    c.execute("select id,comment from bbs where userid = ? order by id", (user_id,))
    comment_list = []
    for row in c.fetchall():
        comment_list.append({"id": row[0], "comment": row[1]})

    c.close()
    return template('bbs' , user_name = user_name , comment_list = comment_list)


#スレッドにリストを追加する
@route('/add_list', method=["POST"])
def add_list():
        # クッキーから user_id を取得
    user_id = request.get_cookie("user_id", secret=secret_cookie)
    
    # 時間を取得、フォーマットを編集
    # time = datetime.now().strftime('%Y/%m/%d %H:%M:%S')
    timeis = datetime.datetime.now()
    created_time = timeis.strftime('%Y/%m/%d %H:%M:%S')

    # POSTアクセスならDBに登録する
    # フォームから入力されたアイテム名の取得(Python2ならrequest.POST.getunicodeを使う)
    comment = request.POST.getunicode("comment")
    thread_id = request.POST.getunicode("title_id")
    conn = sqlite3.connect('service.db')
    c = conn.cursor()
    # DBにデータを追加する
  
    # listに書き出し
    print("comment", comment)
    print("thread_id", thread_id)

    c.execute("insert into lists values(null,?,?,?,?,0,0)", (thread_id,comment,user_id,created_time))
    conn.commit()
    conn.close()
    redirect('/list/'+thread_id)
# @route('/edit/<id:int>')
# def edit(id):
#     conn = sqlite3.connect('service.db')
#     c = conn.cursor()
#     c.execute("select comment from bbs where id = ?", (id,) )
#     comment = c.fetchone()
#     conn.close()

#     if comment is not None:
#         # None に対しては インデクス指定できないので None 判定した後にインデックスを指定
#         comment = comment[0]
#         # "りんご" ○   ("りんご",) ☓
#         # fetchone()で取り出したtupleに 0 を指定することで テキストだけをとりだす
#     else:
#         return "アイテムがありません" # 指定したIDの name がなければときの対処

#     item = { "id":id, "comment":comment }

#     return template("edit", comment=item)


# # /add ではPOSTを使ったので /edit ではあえてGETを使う
# @route("/edit")
# def update_item():
#     # ブラウザから送られてきたデータを取得
#     item_id = request.GET.getunicode("item_id") # id
#     item_id = int(item_id)# ブラウザから送られてきたのは文字列なので整数に変換する
#     comment = request.GET.getunicode("comment") # 編集されたテキストを取得する

#     # 既にあるデータベースのデータを送られてきたデータに更新
#     conn = sqlite3.connect('service.db')
#     c = conn.cursor()
#     c.execute("update bbs set comment = ? where id = ?",(comment,item_id))
#     conn.commit()
#     conn.close()

#     # アイテム一覧へリダイレクトさせる
#     return redirect("/bbs")


# # /del/100 -> item_id = 100
# # /del/50 -> item_id = 50
# # /del/one -> HTTPError 404 文字列にするとエラーが出る
# # /del/stacafe -> HTTPError 404
# # /del/koza -> HTTPError 404
# @route('/del' ,method=["POST"])
# def del_task():
#     id = request.POST.getunicode("comment_id")
#     id = int(id)
#     conn = sqlite3.connect("service.db")
#     c = conn.cursor()
#     c.execute("delete from bbs where id = ? and user_id = ?" ,(id,))
#     conn.commit()
#     c.close()
#     return redirect("/bbs")

@error(403)
def mistake403(code):
    return 'There is a mistake in your url!'

@error(404)
def mistake404(code):
    return '404だよ!URL間違ってない！？'



@route('/static/<filepath:path>')
def server_static(filepath):
    return static_file(filepath, root='static')

# run(port="8081")
run(host="0.0.0.0", port=int(os.environ.get("PORT", 5000)))
