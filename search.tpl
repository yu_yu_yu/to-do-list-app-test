%rebase("base.tpl",title="Search Result")

<article>
    <h2 class="content-title" >検索結果: {{search_count}}件 "{{search_word}}"</h2>
    % if not title_info:
        <div class="container">
            <div class="thread_top_container">
                    <div class="thread_title_container">
                        <h2>お探しのスレッドはありません</h2>
                    </div>
                </div>
            <img src="/static/img/aisatsu_kodomo_boy.png" alt="" class="gomen">
                
        </div>
    % else: 
        % for item in title_info:
        <div class="container">
            <div class="thread_top_container">
                <div class="thread_title_container">
                    <h2>{{item['list_title']}}</h2>
                </div>
            </div>
            <div class="more-detail-link">
                <a  href="/list/{{item['id']}}">もっと見る…</a>
            </div>
                
        </div>
        % end
    % end

</article>