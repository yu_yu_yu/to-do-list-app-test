%rebase("base.tpl",title="To Do List")

	<article>
		
		% for item in title_info:
		<div class="container">
			<div class="thread_top_container">
				<div class="thread_icon_container">
					<img src="/static/img/{{item['user_img_path']}}" alt="">
				</div>
				<div class="thread_title_container">
					<h2>{{item['list_title']}}</h2>
				</div>
			</div>

			% if item["thraed_img_path"] !="":
				<img class = "thread_img" src="/static/img/{{item['thraed_img_path']}}"  alt="">
			% end
			<p>Created: {{item['created_time']}}</p>

			% if item["list_content"] !="":
			<div class="thread_list_container">
				<ul>
					% for content in item["list_content"]:
						<p>{{content}}</p>
					% end
					<a class="more-detail-link" href="/list/{{item['id']}}">もっと見る…</a>
				</ul>
				
			</div>
			
		</div>
		% end
		

	</article>
